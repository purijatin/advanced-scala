package examples

object ImplicitTest {

  implicit class StringExtra(n:String) {
    def asSqrt: Double = math.sqrt(n.toDouble)
  }

  def main(args: Array[String]): Unit = {
    val i: Double = "4".asSqrt
    println(i)
  }
  val a = (1 -> 2)
}
