package examples

import java.util.concurrent.Executors

import scala.concurrent.{ExecutionContext, Future}
import scala.util.{Failure, Success}

object Grep1 {

  import java.io.File

  def search(word: String, file: File)
            (implicit exec: ExecutionContext): Future[Boolean] = Future {
    val content = scala.io.Source.fromFile(file)
    content.getLines().exists(_ contains word)
  }

  def main(args: Array[String]): Unit = {
    implicit val exec = ExecutionContext.fromExecutor(Executors.newCachedThreadPool())

    val ans = search("hello", new File("/home/user/someFile.txt"))
    ans onComplete {
      case Success(x) => println(s"Found: $x")
      case Failure(e) => println(s"Failure ${e.getMessage}")
    }

    ans onComplete (x => println("Result from second onComplete: " + x))

  }
}
