package examples

import scala.concurrent.{Await, ExecutionContext, Future}

object FutureKoans {

  /**
    * Returns a future that is completed with the value of the number passed
    *
    * Hint: Look at interesting methods of the companion object of Future
    *
    * @param n number
    * @return
    */
  def completeAs(n: Int)(implicit e: ExecutionContext): Future[Int] = ???

  /**
    * Returns a future that gets completed with the successful value of the string passed
    *
    * @param n string
    * @return
    */
  def toInt(n: String)(implicit e: ExecutionContext): Future[Int] = ???

  /**
    * Returns the square root of the value of the future
    *
    * @param f
    * @param e
    * @return
    */
  def sqrt(f: Future[Int])(implicit e: ExecutionContext): Future[Double] = ???


  /**
    * Returns the sum of values of two variables that are passed
    *
    * @param a first future
    * @param b second future
    * @return
    */
  def sum(a: Future[Int], b: Future[Int])(implicit e: ExecutionContext): Future[Int] = ???

  /**
    * Returns a future that will be completed as a tuple with the values of the arguments passed
    * @param a
    * @param b
    * @return
    */
  def merge(a: Future[Int], b: Future[Int])(implicit e: ExecutionContext):  Future[(Int, Int)] = ???

  /**
    * Returns an `Option` with the value if the future is successfully completed.
    * If future failed as exception, then returns None
    * If not yet completed, returns None
    * @param f future
    * @tparam T type of future
    * @return
    */
  def asOpt[T](f: Future[T]): Option[T] = ???

  /**
    * Return the future that contains the average of every future passed
    *
    * @param ls list of futures
    * @param e
    * @return
    */
  def average(ls: List[Future[Int]])(implicit e: ExecutionContext): Future[Double] = ???


}
