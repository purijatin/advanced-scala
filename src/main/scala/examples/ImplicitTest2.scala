package examples

object ImplicitTest2 {
  def main(args: Array[String]): Unit = {
    question1()
    question2()
  }


  def question1(): Unit = {
    /**
      * Question:
      * * Uncomment the below line, and make sure it compiles.
      * * The below expression should return: india - hi
      */
    //    val a = "hi".append("india")
    //    assert(a == "india -  hi")

    //    val b = "hello".append("Bengaluru")
    //    assert(b == "Bengaluru - hello")
    //    the above should return: Bengaluru - hello
  }


  def question2(): Unit = {
    val list = List(1, 2, 3)

    /**
      * Uncomment the below line.
      *
      * Aim:
      * * Make sure the program  compiles
      * * When `asJava` is called on immutable scala list, it should return a java.util.List with the same contents,
      * and in the same order as original scala collection
      */
//    val asJavaList: java.util.List[Int] = list.asJava
//    assert(asJavaList == java.util.Arrays.asList(1, 2, 3))
//    assert(List[Int]().asJava == new util.ArrayList[Int]())
  }

}
