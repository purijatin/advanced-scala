package examples.stddev

import scala.concurrent.{ExecutionContext, Future}

object MeanStdDev2 {

  /**
    * Takes a list of statistics. And returns the mean of the (mean of all statistics)
    * @param ls list
    * @param e execution context
    * @return
    */
  def netMean(ls: List[Statistics])(implicit e: ExecutionContext): Future[Double] = ???

  /**
    * The mean obtained above, cannot be quantified as the mean of all the files.
    * The correct solution would be, that we obtain the mean based on the clubbed  records of all the files
    *
    * The program takes list of statistics. And returns the statistics of all the statistics merged
   */
  def multipleLogs(ls: List[Statistics])(implicit e: ExecutionContext): Statistics = ???

}
