package examples

abstract class Monoid[A] {
  def add(x: A, y: A): A
  def unit: A
}

object Monoid{
  implicit val stringMonoid: Monoid[String] = new Monoid[String] {
    def add(x: String, y: String): String = x concat y
    def unit: String = ""
  }

  implicit val intMonoid: Monoid[Int] = new Monoid[Int] {
    def add(x: Int, y: Int): Int = x + y
    def unit: Int = 0
  }
}

class MonoidTest {

  /**
    * Implement the below function `sum`, that sums up the list. The behavior of adding two elements, is obtained from `Monoid`
    * @param ls list
    * @tparam A type of list
    */
  def sum[A](ls:List[A])(implicit m: Monoid[A]): A = ???

  def main(args: Array[String]): Unit = {
    assert(sum(List(1,2,3)) == 6)
    assert(sum(List("hi","hello")) == "hihello")
    assert(sum(List.empty[Int]) == 0)
    assert(sum(List.empty[String]) == "")
  }
}
