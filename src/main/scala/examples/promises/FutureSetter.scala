package examples.promises

import scala.concurrent.{Future, Promise}
import scala.util.Try

class FutureSetter[T] {

  /**
    * Sets the value of the object
    * @param n
    */
  def set(n: Try[T]): Unit = ???

  /**
    * Gets the future. That would be completed with the value, as set by `set(..)` method
    * @return
    */
  def get: Future[T] = ???
}
