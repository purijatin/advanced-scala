package examples

case class Student(name: String, rollNo: Int)

object OrderTest {
  def main(args: Array[String]): Unit = {
    val sachin = Student("sachin", 1)
    val virat = Student("virat", 2)
    val dhoni = Student("dhoni", 3)

    val ls = List(virat, dhoni, sachin)

    /*
      Please uncomment the below lines.
      Question:
        1) Make sure the program compiles
        2) And the assertion matches
     */
    //assert(ls.sorted == List(sachin, virat, dhoni))

    /*
      Is there a way that we can sort `ls` in lexicographical ordering rather than by roll number?
     */
  }
}
