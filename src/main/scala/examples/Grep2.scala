package examples

import java.io.File
import java.util.concurrent.Executors

import examples.Grep1.search

import scala.concurrent.{ExecutionContext, Future}
import scala.util.{Failure, Success}


object Grep2 {

  /**
    * Searches for the word in all the files (and in nested directory) of the root folder provided
    * @param word the word to search
    * @param root the root directory
    * @param exec execution context
    * @return
    */
  def searchInAll(word: String, root: File)
                 (implicit exec: ExecutionContext): Future[Seq[File]] = ???

  /**
    * searches the `word` in the argument `file`
    * @param word word to search
    * @param file the file in which to search
    * @param exec the execution context
    * @return A future which will be completed with true, if the word was found in the file
    */
  def search(word: String, file: File)
            (implicit exec: ExecutionContext): Future[Boolean] = Future {
    val content = scala.io.Source.fromFile(file)
    content.getLines().exists(_ contains word)
  }

}
