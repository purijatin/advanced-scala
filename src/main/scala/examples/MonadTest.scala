package examples


trait Monad[F[_]] {
  /**
    * Returns a unit of `F[A]`. Its like a wrapper of single element
    *
    * @param a
    * @tparam A
    * @return
    */
  def unit[A](a: A): F[A]

  def flatMap[A, B](a: F[A], f: A => F[B]): F[B]

  def map[A, B](a: F[A], f: A => B): F[B] = flatMap(a, (x: A) => unit(f(x)))
}

object Monad {
  implicit val listMonad: Monad[List] = new Monad[List] {
    override def unit[A](a: A): List[A] = List(a)
    override def flatMap[A, B](a: List[A], f: A => List[B]): List[B] = a.flatMap(f)
  }

  implicit val optionMonad: Monad[Option] = new Monad[Option] {
    override def unit[A](a: A): Option[A] = Option(a)
    override def flatMap[A, B](a: Option[A], f: A => Option[B]): Option[B] = a.flatMap(f)
  }
}


object MonadTest {

  def asString[A[_]: Monad](elem: A[Int]): A[String] = {
    val ev = implicitly[Monad[A]]
    ev.map(elem, (x:Int) => x.toString)
  }

  def main(args: Array[String]): Unit = {
    val ls = List(1,2,3)
    println(asString(ls))
    println(asString(Option(1)))
  }
}

