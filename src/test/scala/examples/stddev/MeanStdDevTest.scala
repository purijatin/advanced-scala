package examples.stddev

import java.io.{BufferedWriter, File, FileOutputStream, OutputStreamWriter}

import org.scalactic.TolerantNumerics
import org.scalatest.{Matchers, WordSpec}

import scala.concurrent.Await
import scala.concurrent.duration._
import scala.io.Source

class MeanStdDevSpec extends WordSpec with Matchers {
  import scala.concurrent.ExecutionContext.Implicits._

  //  val b = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(new File("/home/jatin/work/trainings/affine/affine-scala-code/code/src/main/resources/runtime.txt"))))
  //
  //  for{
  //    _ <- 1 to 1000
  //    n = Random.nextDouble() * 100
  //  } {
  //    b.write(n+"sec\n")
  //  }
  //  b.close()

  "Statistics" should {
    "calculate mean and std dev" in {
      val file = new File("temp.txt")
      file.deleteOnExit()
      val b = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file)))
      for {
        _ <- 1 to 1000
        n = 1
      } {
        b.write(n + "sec\n")
      }
      b.close()

      val s = new StatisticsCalculator(Source.fromFile(file))
      Await.result(s.getMean, 5 seconds) shouldEqual 1
      Await.result(s.variance, 5 seconds) shouldEqual 0
    }

    "calculate mean and std dev - better example" in {
      val file = new File("temp.txt")
      file.deleteOnExit()
      val b = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file)))
      for {
        n <- 1 to 10
      } {
        b.write(n + "sec\n")
      }
      b.close()

      implicit val doubleEquality = TolerantNumerics.tolerantDoubleEquality(0.001)

      val s = new StatisticsCalculator(Source.fromFile(file))
      assert(Await.result(s.getMean, 5 seconds) === 5.5)
      assert(Await.result(s.variance, 5 seconds) === 8.25)
      assert(Await.result(s.stdDev, 5 seconds) === 2.87228)
    }
  }

}