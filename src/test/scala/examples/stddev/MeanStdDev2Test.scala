package examples.stddev

import org.scalatest.{Matchers, WordSpec}

import scala.concurrent.duration._
import scala.concurrent.{Await, Future}


class MeanStdDev2Test extends WordSpec with Matchers {
  "netMean" should {
    "give a mean of means" in {

      import scala.concurrent.ExecutionContext.Implicits._

      val a = getSampleStatistics(List(1.0, 2, 3))
      val b = getSampleStatistics(List(0.0))
      val c = getSampleStatistics(List(2.0))

      val ans = MeanStdDev2.netMean(List(a, b, c))
      assert(Await.result(ans, 2 seconds) === 4/3.0)
    }
  }


  "multipleLogs" should {
    "give correct statistics" in {

      import scala.concurrent.ExecutionContext.Implicits._

      val a = getSampleStatistics(List(1.0, 2, 3))
      val b = getSampleStatistics(List(0.0))
      val c = getSampleStatistics(List(2.0))

      val ans = MeanStdDev2.multipleLogs(List(a, b, c))
      assert(Await.result(ans.getMean, 2 seconds) === 8/5.0)
    }
  }

  def getSampleStatistics(ls: List[Double]) = new  Statistics {
    override def all: Future[List[Double]] = Future.successful(ls)
  }
}
