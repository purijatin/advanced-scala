package examples

import examples.promises.FutureSetter
import org.scalatest.{Matchers, WordSpec}

import scala.concurrent.Await
import scala.concurrent.duration._
import scala.util.Try

class FutureSetterTest extends WordSpec with Matchers {

  "FutureSetter" should {
    "set multiple times" in {
      val f = new FutureSetter[Int]()
      f.set(Try(1))
      f.set(Try(2))

      assert(Await.result(f.get, 2 seconds) === 1)
    }

    "set second time" in {
      val f = new FutureSetter[Int]()
      f.set(Try(1))
      f.set(Try(throw new RuntimeException))

      assert(Await.result(f.get, 2 seconds) === 1)
    }

    "set fail" in {
      val f = new FutureSetter[Int]()
      f.set(Try(throw new RuntimeException))

      try {
        Await.ready(f.get, 2 seconds)
        fail()
      } catch {
        case e =>
      }
    }
  }
}
