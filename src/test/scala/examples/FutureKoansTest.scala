package examples

import org.scalatest.{Matchers, WordSpec}

import scala.concurrent.Future.successful
import scala.concurrent.{Await, Future, Promise}
import scala.concurrent.duration._
import scala.util.Try

class FutureKoansTest extends WordSpec with Matchers {
  import scala.concurrent.ExecutionContext.Implicits._

  "Koans" should {
    "completeAs" in {
      val  f = FutureKoans.completeAs(1)
      assert(Await.result(f, Duration.Zero) === 1)
    }

    val d = 2 seconds

    "toInt" in {
      val  f = FutureKoans.toInt("1")
      assert(Await.result(f, d) === 1)
      assert(Try(Await.result(FutureKoans.toInt("hi"), d)).isSuccess === false)
    }

    "sqrt" in {
      assert(Await.result(FutureKoans.sqrt(successful(4)), d) === 2.0)
      assert(Await.result(FutureKoans.sqrt(successful(0)), d) === 0.0)
    }

    "sum" in {
      assert(Await.result(FutureKoans.sum(successful(4), successful(5)), d) === 9)
      assert(Await.result(FutureKoans.sum(successful(0), successful(0)), d) === 0)
    }

    "merge" in {
      assert(Await.result(FutureKoans.merge(successful(4), successful(5)), d) === (4,5))
      assert(Await.result(FutureKoans.merge(successful(0), successful(0)), d) === (0,0))
    }

    "asOpt" in {
      assert(FutureKoans.asOpt(successful(4)) === Option(4))
      assert(FutureKoans.asOpt(Future.failed(new RuntimeException)) === None)
      assert(FutureKoans.asOpt(Promise[Int]().future) === None)
    }


  }
}
